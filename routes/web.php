<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();

Route::get('/home', 'LaravelController@home')->name('home');

Route::get('/signup', 'LaravelController@signupForm')->name('signup.form');
Route::post('signup', 'LaravelController@store')->name('signup.store');

Route::get('/signin', 'LaravelController@signinForm')->name('signin.form');
Route::post('attempt', 'LaravelController@attempt')->name('signin.attempt');

Route::get('/logout', 'LaravelController@logout');
